<?php

class TCAModelTest extends PHPUnit_Framework_TestCase {

	/** @var \PS\Extempore\Modeler\TCAModel */
	protected $model;

	public function setup() {
		$tca = new \PS\Extempore\TCAClient('myext');
		$this->model = $tca->model('partner');
	}

	/**
	 * @test
	 */
	public function itCreatesAModel() {
		$this->assertInstanceOf('\PS\Extempore\Modeler\Model', $this->model);
	}

	/**
	 * @test
	 * @depends itCreatesAModel
	 */
	public function itCreatesAStringField() {
		$field = $this->model->string('test');
		$this->assertInstanceOf('\PS\Extempore\Field\String', $field);
	}

	/**
	 * @test
	 * @depends itCreatesAStringField
	 */
	public function itAddsFieldsToCurrentPalette() {
		$this->model->forPalette('mypalette');
		$this->model->string('hello');
		$this->assertContains('hello', array_keys($this->model->palettes['mypalette']));
	}

	/**
	 * @test
	 * @depends itCreatesAStringField
	 */
	public function itAddsFieldsToCurrentTab() {
		$this->model->forTab('mytab');
		$this->model->string('hello');
		$this->assertContains('hello', array_keys($this->model->tabs['mytab']));
	}

	/**
	 * @test
	 * @depends itCreatesAStringField
	 */
	public function itRetainsAReferenceToFields() {
		$field = $this->model->string('test');
		$this->assertSame($this->model->fields['test'], $field);
	}

	/**
	 * @test
	 * @depends itCreatesAStringField
	 */
	public function itAcceptsFieldLabels() {
		$field = $this->model->string(array('test' => 'Test'));
		$this->assertEquals('Test', $this->model->labels['field']['test']);
	}

	/**
	 * @test
	 * @depends itCreatesAModel
	 */
	public function itAcceptsModelLabels() {
		$tca = new \PS\Extempore\TCAClient('myext');
		$this->model = $tca->model(array('partner' => 'Test Partner'));
		$this->assertEquals('Test Partner', $this->model->labels['model']);
	}

	/**
	 * @test
	 * @depends itAddsFieldsToCurrentPalette
	 */
	public function itAcceptsPaletteLabels() {
		$this->model->forPalette(array('contact' => 'Contact Information'));
		$this->assertEquals('Contact Information', $this->model->labels['palette']['contact']);
	}

	/**
	 * @test
	 * @depends itAddsFieldsToCurrentTab
	 */
	public function itAcceptsTabLabels() {
		$this->model->forTab(array('contact' => 'Contact Information'));
		$this->assertEquals('Contact Information', $this->model->labels['tab']['contact']);
	}

	/**
	 * @test
	 * @depends itCreatesAStringField
	 */
	public function itMaintainsFieldOrder() {
		$fields = array('one', 'two', 'three');
		foreach ($fields as $f) {
			$this->model->string($f);
		}
		$actualOrder = array();
		foreach ($this->model->orderedElements[1] as $element) {
			$actualOrder[] = $element['key'];
		}
		$this->assertSame($fields, $actualOrder);
	}

	/**
	 * @test
	 * @depends itMaintainsFieldOrder
	 */
	public function itMaintainsSomewhatComplicatedOrder() {
		$this->model->forType('partner');
		$this->model->string('one');
		$this->model->forPalette('two');
		$this->model->string('three');
		$this->model->endPalette();
		$this->model->string('four');
		$this->model->forTab('five');
		$this->model->string('six');
		$this->model->forPalette('seven');
		$this->model->string('seven');
		$this->model->string('eight');
		$this->model->endPalette();
		$this->model->string('nine');
		$this->model->endTab();
		$this->model->string('ten');
		$expectedKeys = array('one','two','three','four','five','six','seven','eight','nine','ten');
		$resultOrder = array();
		foreach ($this->model->orderedElements['partner'] as $element) {
			$resultOrder[] = $element['key'];
		}
		$this->assertSame($expectedKeys, $resultOrder);
	}

	/**
	 * @test
	 */
	public function itAcceptsCustomTypeField() {
		$this->model->typeField('ilk');
		$this->assertEquals('ilk', $this->model->typeField);
	}

	/**
	 * @test
	 * @depends itAcceptsCustomTypeField
	 */
	public function itAcceptsTypeFieldLabels() {
		$label = 'What kind of partner is this?';
		$this->model->typeField(array('kind' => $label));
		$this->assertEquals($label, $this->model->labels['typeField']);
	}


	public function itDoesntAllowPalettesWithinPalettes() {}
	public function itDoesntAllowTabsWithinTabs() {}
	public function itDoesntAllowTabsWithinPalettes() {}

}