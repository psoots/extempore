<?php

class TCAClientTest extends PHPUnit_Framework_TestCase {

	/** @var \PS\Extempore\TCAClient */
	protected $tca;

	public function setup() {
		$this->tca = new \PS\Extempore\TCAClient('myext');
	}

	/**
	 * @test
	 */
	public function itCanBeInstatiated() {
		$this->assertInstanceOf('\PS\Extempore\TCAClient', $this->tca);
	}

}