# Extempore

Extempore is a no-thinking extension manager for TYPO3 and ExtBase.

### ext_localconf.php

Everything stems from a single Extempore TCA Client.
```php
$tca = new \PS\Extempore\TCAClient('asperacp');
```

Models are created all in the same file. 
```php
$model = $tca->model('Partner');
```

Fields are created directly on this model object. They are rendered in the TCA in the order that they are created. The simplest model has no palettes and only a single type:
```php
$model->string('firstName');
$model->string('lastName');
$model->link('www');
$tca->done();
```

This would create a model in the TCA with the three properties, in the order of creation. Each field has practical defaults already defined. And all fields are added to the default type, 1, for this model.

Methods are provided to the fields for overriding the defaults:
```php
$model->string('firstName')->required()->size(30);
```

When you need to create models with different types, you simply declare which type you are adding fields to:
```php
$model->forType('organization');
$model->text('patents')->rows(5)->cols(40);
$model->image('logo');
```

That will add the fields 'patents' and 'logo' to only the organization type. Fields defined before the first `forType()` call will be added to the default type. This isn't ideal. If you are going to defined types for a model, you should use explicit types. And there is no `endType()` call.

Also when you define types, you can specify the name of the field which is used to set the type on the model. By default, a 'kind' field is used. The type setting field is always rendered first on the form, since it's used  to determine which fields are visible to the user.
```php
$model->typeField('ilk');
$model->typeField(array('ilk' => 'What type of Partner is this?'));
```

To preserve field order when defining fields for multiple types, it may be easier to specify the type for each field.
```php
$model->forType('organization')
	->text('patents')
	->rows(5)
	->cols(40);
$model->forTypes(array('organization','individual'))
	->string('facebook');
$model->forType('organization')
	->image('logo');
```

Palettes are declared nearly in the same way that types are. Like fields, they are added to the current type(s) only.
```php
$model->forType('individual');
$model->forPalette('contact');
$model->string('firstName');
$model->string('lastName');
$model->paletteLineBreak();
$model->email('email');
$model->endPalette();
```

Palettes are shown in the order that they are declared, unless they are used later on by the same model type:
```php
$model->forType('individual');
$model->forPalette('contact');
// ...
$model->endPalette();
$model->richText('description');
$model->usePalette('contact'); 
// ...
$model->forType('organization');
$model->usePalette('contact');
$model->text('patents');
```

This declares the contact palette on the 'individual' type, but it is render after the 'description' field even though it was declared before it. The contact palette is used again by the 'organization' type and rendered before the 'patents' field.

By default, labels will be sourced to a locallang file. However, you may want to declare labels directly in the TCA:
```php
$model->string(array('firstName' => 'First Name'));
```
```php
$model->forType(array('individual' => 'Individual Partner'));
```
```php
$model->forPalette(array('contact' => 'Contact Information'));
```
```php
// Only works for non-numeric keys
$model->forTypes(array(
   'individual' => 'Individual Partner',
   'organization' => 'Organization Partner'));
```
```php
$model = $tca->model(array('partner' => 'Foundation Partner'));
```

Once a label has been specified, there is no need to re-declare the label again:
```php
$model->forTypes(array('individual','organization'))->usePalette('contact');
```

However, you could re-declare the labels whenever, in case you want to  isolate the label definitions.

Tabs are defined the same way as palettes and like palettes rendered in the order that it is defined unless used by the same model type later with `$model->useTab('background')`.
```php
$model->forTab('background');
$model->richText('history');
$model->endTab();
```

By default, all models get an 'admin' tab rendered at the end with fields: 'langauge', 'starttime', 'endtime', and 'hidden'. You can turn off this admin tab:
```php
$model->noAdminTab();
```

ExtBase models have some standard fields like: 'crdate', 'tstamp', 'language', 'starttime', 'endtime', etc. These fields are added automatically and you cannot override them with the Extempore TCAClient. However, as the TCAClient simply writes to the TCA, you can make any modifications after calling `$tca->done()`, by modifying the TCA directly:
```php
$TCA['tx_myext_domain_model_mymodel']['columns']['tstamp']['readonly'] = 0;
``` 

Since most of these fields are not rendered on a TCA form or rendered only in the Admin tab, you can add them to the form by doing this between field definitions:
```php
$model->useField('language');
```

You can also use that call with other custom fields too when you want to reuse fields for different model types without having to restructure your field definitions. This can can be called without or without the field being defined yet. However, no custom field is rendered without a definition. 

Since models are declared all in the same file, we can specify the relations immediately.

```php
$partner = $tca->model('Partner');
$category = $tca->model('Category');
$solution = $tca->model('Solution');
$partner->hasMany('categories', $category);
$partner->hasOne('solution', $solution);
$category->belongsTo('partner', $partner);
$solution->belongsTo('partner', $solution);
```

You may need to reference an existing table:
```php
$partner->hasMany('owners', 'fe_users');
```

You can also inherit from existing tables:
```php
$user = $tca->model(array('FrontendUser' => 'Website User'), 'fe_users');
```

Fields declared on this model will be added to the end of the FrontendUser form. You may want to make extensive use of the `useField()` call to  reorganize the order of fields on the form.

Another way to define a palette (no need to call `endPalette()` here). It's vital that you modify the $model as a reference when using callbacks!!
```php
$model->forPalette('contact', function(&$model{
	$model->string('firstName')->required()->size(20);
	$model->string('lastName')->required()->size(20);
	$model->email('email')->required()->size(20);
	$model->paletteLineBreak();
	$model->text('address')->required()->size(20);
	$model->string('phone')->required()->size(20);
});
```

Another way to define the fields for a type. This is only a convenience for visually offsetting which fields belong to which type. There is no `endType()` method, so the current type will be set until another `forType()` method is called.
```php
$model->forType('one', function(&$model){
	$model->usePalette('contact');
	$model->link('url')->required();
	$model->string('twitter');
	$model->string('facebook');
});
```

Adding fields to multiple types:
```php
$model->forTypes(array('one', 'two'), function(&$model)) {
	$model->text('teaser')->cols(40)->rows(15);
	$model->richText('content')->noTrim();
	$model->usePalette('contact');
}
```

And all is for naught if you don't call `done()`!
```php
$tca->done();
```