<?php

namespace PS\Extempore\Field;

class String extends AbstractField {

	/** @var int */
	protected $size = 10;

	/** @var array */
	protected $evals = array('trim');


	/**
	 * @param int $size
	 * @return $this
	 */
	public function &size($size) {
		$this->size = $size;
		return $this;
	}

	public function &noTrim() {
		$key = array_search('trim', $this->evals);
		if ($key !== FALSE) unset($this->evals[$key]);
		return $this;
	}

}