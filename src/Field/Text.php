<?php

namespace PS\Extempore\Field;

class Text extends AbstractField {

	/** @var int */
	protected $cols = 40;

	/** @var int */
	protected $rows = 15;

	/** @var array */
	protected $evals = array('trim');


	/**
	 * @param int $rows
	 * @return $this
	 */
	public function &rows($rows) {
		$this->rows = $rows;
		return $this;
	}

	/**
	 * @param int $cols
	 * @return $this
	 */
	public function &cols($cols) {
		$this->cols = $cols;
		return $this;
	}

	/**
	 * Turns off trim when evaluating this field
	 *
	 * @return $this
	 */
	public function &noTrim() {
		$key = array_search('trim', $this->evals);
		if ($key !== FALSE) unset($this->evals[$key]);
		return $this;
	}


}