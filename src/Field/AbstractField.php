<?php

namespace PS\Extempore\Field;

abstract class AbstractField {

	/** @var string */
	protected $name;

	/** @var array */
	protected $evals = array();


	/** Constructor */
	public function __construct($name) {
		$this->name = $name;
	}


	/**
	 * @param bool $bool
	 * @return $this
	 */
	public function &required($bool = TRUE) {
		if ($bool) {
			$this->evals[] = 'required';
		}
		return $this;
	}


	/**
	 * Secret properties to clients,
	 * but public to partners in the know.
	 *
	 * @param string $prop
	 * @return mixed|string
	 */
	public function __get($prop) {
		return $this->$prop;
	}
}