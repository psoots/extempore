<?php

namespace PS\Extempore\Modeler;

class Modeler {
	
	/** @var string **/
	protected $extension;

	/** @var array */
	protected $models = array();

	/** Constructor */
	public function __construct($extension) {
		$this->extension = $extension;
	}

	/**
	 * Creates a model object for this extension
	 *
	 * @param string $name
	 * @return \PS\Extempore\Modeler\Model
	 */
	public function &model($name) {
		$model = new Model($name);
		$this->models[] = $model;
		return $model;
	}

	/**
	 * Secret properties to clients,
	 * but public to partners in the know.
	 *
	 * @param string $prop
	 * @return mixed|string
	 */
	public function __get($prop) {
		switch ($prop) {
			case 'models': return $this->models;
			case 'extension': return $this->extension;
		}
	}
}