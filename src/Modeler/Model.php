<?php

namespace PS\Extempore\Modeler;

class Model {

	/** @var string */
	protected $name;

	/** @var array */
	protected $fields = array();

	/** @var \PS\Extempore\Factory\FieldFactory */
	protected $fieldFactory;

	/**
	 * @param string $name
	 */
	public function __construct($name) {
		$this->name = $name;
		$this->fieldFactory = new \PS\Extempore\Factory\FieldFactory();
	}

	/** @return \PS\Extempore\Field\String */
	public function &string($fieldName) {
		return $this->field(__FUNCTION__, $fieldName);
	}

	/** @return \PS\Extempore\Field\Number */
	public function &number($fieldName) {
		return $this->field(__FUNCTION__, $fieldName);
	}

	/** @return \PS\Extempore\Field\Link */
	public function &link($fieldName) {
		return $this->field(__FUNCTION__, $fieldName);
	}

	/** @return \PS\Extempore\Field\Image */
	public function &image($fieldName) {
		return $this->field(__FUNCTION__, $fieldName);
	}

	/** @return \PS\Extempore\Field\Text */
	public function &text($fieldName) {
		return $this->field(__FUNCTION__, $fieldName);
	}

	/** @return \PS\Extempore\Field\RichText */
	public function &richText($fieldName) {
		return $this->field(__FUNCTION__, $fieldName);
	}

	/** @return \PS\Extempore\Field\File */
	public function &file($fieldName) {
		return $this->field(__FUNCTION__, $fieldName);
	}

	/** @return \PS\Extempore\Field\HasMany */
	public function &hasMany($fieldName, \PS\Extempore\Field\AbstractField $foreignField) {
		return $this->relationField(__FUNCTION__, $fieldName, $foreignField);
	}

	/** @return \PS\Extempore\Field\HasOne */
	public function &hasOne($fieldName, \PS\Extempore\Field\AbstractField $foreignField) {
		return $this->relationField(__FUNCTION__, $fieldName, $foreignField);
	}

	/**
	 * @param string $style
	 * @param string $fieldName
	 * @return \PS\Extempore\Field\AbstractField
	 */
	protected function &field($style, $fieldName) {
		$field = $this->fieldFactory->create($style, $fieldName);
		$this->fields[$fieldName] = $field;
		return $field;
	}

	/**
	 * @param string $style
	 * @param string $fieldName
	 * @param \PS\Extempore\Field\AbstractField $foreignField
	 * @return \PS\Extempore\Field\AbstractField
	 */
	protected function &relationField($style, $fieldName, \PS\Extempore\Field\AbstractField $foreignField) {
		$field = $this->fieldFactory->create($style, $fieldName, $foreignField);
		$this->fields[$fieldName] = $field;
		return $field;
	}

	/**
	 * Secret properties to clients,
	 * but public to partners in the know.
	 *
	 * @param string $prop
	 * @return mixed|string
	 */
	public function __get($prop) {
		switch ($prop) {
			case 'fields': return $this->fields;
			case 'name': return $this->name;
		}
	}
}
