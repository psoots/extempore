<?php

namespace PS\Extempore\Modeler;

class TCAModel extends Model {

	/** @var array */
	protected $currentTypes = array(1);

	/** @var string */
	protected $currentPalette = '';

	/** @var string */
	protected $currentTab = '';

	/** @var array */
	protected $types = array();

	/** @var array */
	protected $palettes = array();

	/** @var array */
	protected $tabs = array();

	/** @var string */
	protected $typeField = '';
	
	/** @var array */
	protected $orderedElements = array();

	/** @var array */
	protected $labels = array(
		'palette' => array(),
		'type' => array(),
		'tab' => array(),
		'field' => array(),
		'typeField' => '',
		'model' => ''
	);

	/**
	 * The name of the model. You can also set the label
	 * by passing an array: array('mymodel' => 'My Awesome Model')
	 *
	 * @param string|array $name
	 */
	public function __construct($name) {
		$modelKey = $this->setLabel('model', $name);
		parent::__construct($modelKey);
	}

	/**
	 * Set the current types for the next added fields
	 * @param array $types
	 * @return $this
	 */
	public function &forTypes(array $types) {
		$typeKeys = array();
		foreach ($types as $key => $type) {
			if (!is_numeric($key)) {
				$typeKeys[] = $this->setLabel('type', array($key => $type));
			} else {
				$typeKeys[] = $type;
			}
		}
		$this->currentTypes = $typeKeys;
		return $this;
	}

	/**
	 * Set the current type for the next added fields
	 *
	 * @param string|array $typeName
	 * @return $this
	 */
	public function &forType($typeName) {
		return $this->forTypes(array($typeName));
	}

	/**
	 * Specifies the name of a field to use for determing
	 * the model type.
	 *
	 * @param string|array $fieldName
	 * @return $this
	 */
	public function &typeField($fieldName) {
		$key = $this->setLabel('typeField', $fieldName);
		$this->typeField = $key;
		return $this;
	}

	/**
	 * Set the current palette for the next added fields
	 *
	 * @param string|array $paletteName
	 * @return $this
	 */
	public function &forPalette($paletteName) {
		$paletteKey = $this->setLabel('palette', $paletteName);
		$this->currentPalette = $paletteKey;
		$this->addToOrderedElements($paletteKey, 'palette');
		return $this;
	}

	/**
	 * After calling this, new fields will not be added
	 * to the current palette.
	 *
	 * @return $this
	 */
	public function &endPalette() {
		$this->currentPalette = '';
		return $this;
	}

	/**
	 * Set the current tab for the next added fields
	 *
	 * @param string|array $tabName
	 * @return $this
	 */
	public function &forTab($tabName) {
		$tabKey = $this->setLabel('tab', $tabName);
		$this->currentTab = $tabKey;
		$this->addToOrderedElements($tabKey, 'tab');
		return $this;
	}

	/**
	 * After calling this, new fields will not be
	 * added to the current tab.
	 *
	 * @return $this
	 */
	public function &endTab() {
		$this->currentTab = '';
		return $this;
	}

		/**
	 * @param string $style
	 * @param string|array $fieldName
	 * @return \PS\Extempore\Field\AbstractField
	 */
	protected function &field($style, $fieldName) {
		$fieldKey = $this->setLabel('field', $fieldName);
		$field = parent::field($style, $fieldKey);
		$this->addFieldToInterface($fieldKey);
		return $field;
	}

	/**
	 * @param string $style
	 * @param string|array $fieldName
	 * @param \PS\Extempore\Field\AbstractField $foreignField
	 * @return \PS\Extempore\Field\AbstractField
	 */
	protected function &relationField($style, $fieldName, \PS\Extempore\Field\AbstractField $foreignField) {
		$fieldKey = $this->setLabel('field', $fieldName);
		$field = parent::relationField($style, $fieldKey, $foreignField);
		$this->addFieldToInterface($fieldKey);
		return $field;
	}

	/**
	 * Adds a newly created field to the current type, palette, or tab.
	 * If there are no current types, palettes, or tabs, then we do nothing.
	 * Those default behavior will happen later.
	 *
	 * @param string $fieldKey
	 */
	protected function addFieldToInterface($fieldKey) {
		// Types
		foreach ($this->currentTypes as $currentType) {
			self::appendToArray($this->types, $currentType, $fieldKey);
		}
		// Palettes
		if ($this->currentPalette) {
			self::appendToArray($this->palettes, $this->currentPalette, $fieldKey);
		}
		// Tabs
		if ($this->currentTab) {
			self::appendToArray($this->tabs, $this->currentTab, $fieldKey);
		}
		$this->addToOrderedElements($fieldKey, 'field');
	}

	/**
	 * Add the element (field, tab, palette) to the orderedElements
	 * array for the current type. If the key already exists for that
	 * interfaceType and in the current type, then it will be removed
	 * from it's current position and added to the end. 
	 *
	 * Order is based on how it works in the TCA 'showitem' value.
	 * 1. There is a different order for each domain type.
	 * 2. If there is no tab or palette
	 *
	 *
	 * @param string $key
	 * @param string $interfaceType 
	 */
	protected function addToOrderedElements($key, $interfaceType) {
		$newOrderedElements = array();
		foreach ($this->currentTypes as $currentType) {
			$newOrderedElements[$currentType] = array();

			// Copy ordered elements without the new element if it exists
			if (isset($this->orderedElements[$currentType])) {
				foreach ($this->orderedElements[$currentType] as $element) {
					if ($element['key'] != $key && $element['interfaceType'] == $interfaceType) {
						$newOrderedElements[$currentType][] = $element;
					}
				}
			}

			// Add the new element at the end
			$newOrderedElements[$currentType][] = array(
				'key' => $key, 
				'interfaceType' => $interfaceType
			);
		}
		$this->orderedElements = $newOrderedElements;
	}

	/**
	 * Label is derived from elsewhere unless specified
	 * at definition time with an array like:
	 * $model->string(array('title' => 'Job Title'))
	 * or
	 * $tca->model(array('partner' => 'Foundation Partner'))
	 *
	 * @param string $target
	 * @param string|array $value
	 * @return string The target name, not label.
	 */
	protected function setLabel($target, $value) {
		if (!is_array($value)) {
			return $value;
		}
		$key = key($value);
		$label = current($value);
		switch ($target) {
			case 'typeField':
			case 'model':
				$this->labels[$target] = $label;
				break;
			default:
				$this->labels[$target][$key] = $label;
		}
		return $key;
	}

	/**
	 * @param array $array
	 * @param string $key
	 * @param mixed $value
	 */
	protected static function appendToArray(array &$array, $key, $value) {
		if (isset($array[$key])) {
			$array[$key][] = $value;
		} else {
			$array[$key] = array($value);
		}
	}

	/**
	 * Secret properties to clients,
	 * but public to partners in the know.
	 *
	 * @param string $prop
	 * @return mixed|string
	 */
	public function __get($prop) {
		switch ($prop) {
			case 'types': return $this->types;
			case 'palettes': return $this->palettes;
			case 'tabs': return $this->tabs;
			case 'labels': return $this->labels;
			case 'typeField': return $this->typeField;
			case 'orderedElements': return $this->orderedElements;
		}
		return parent::__get($prop);
	}

}