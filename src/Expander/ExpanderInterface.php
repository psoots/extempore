<?php

namespace PS\Extempore\Expander;

/**
 * Interface PS\Extempore\Expander\ExpanderInterface
 *
 * An expander takes a complete modeler instance
 * and expands the models into some other form.
 */
interface ExpanderInterface {

	/**
	 * @param \PS\Extempore\Modeler\Modeler $modeler
	 * @return mixed
	 */
	public function expand(\PS\Extempore\Modeler\Modeler $modeler);
}