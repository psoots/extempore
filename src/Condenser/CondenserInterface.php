<?php

namespace PS\Extempore\Condenser;

/**
 * Interface \PS\Extempore\Condenser\CondenserInterface
 *
 * A condenser takes a source and condenses
 * it into a modeler instance with a complete
 * model system created from the source.
 */
interface CondenserInterface {

	/**
	 * @param $source
	 * @return \PS\Extempore\Modeler\Modeler
	 */
	public function condense($source);
}