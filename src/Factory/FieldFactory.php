<?php

namespace PS\Extempore\Factory;

class FieldFactory {


	/**
	 * Creates a new field object dynamically.
	 *
	 * @param string $style
	 * @param string $fieldName
	 * @return \PS\Extempore\Field\AbstractField
	 * @throws \Exception
	 */
	public function create($style, $fieldName) {
		$className = '\\PS\\Extempore\\Field\\'.ucfirst($style);
		if (!class_exists($className)) {
			throw new \Exception("There is no class, $className.");
		}
		return new $className($fieldName);
	}

	/**
	 * Creates a new field object dynamically.
	 *
	 * @param string $style
	 * @param string $fieldName
	 * @param \PS\Extempore\Field\AbstractField $foreignField
	 * @return \PS\Extempore\Field\AbstractField
	 * @throws \Exception
	 */
	public function &createRelation($style, $fieldName, \PS\Extempore\Field\AbstractField $foreignField) {
		$className = '\\PS\\Extempore\\Field\\'.ucfirst($style);
		if (!class_exists($className)) {
			throw new \Exception("There is no class, $className.");
		}
		return new $className($fieldName);
	}


}
