<?php

namespace PS\Extempore;


/**
 * Class TCAClient
 *
 * A useful wrapper class to the Modeler to make
 * writing the models in an ext_localconf.php file
 * easier. User creates the models and then calls our
 * special 'done()' method which updates the TCA.
 */
class TCAClient extends Modeler\Modeler {

	/**
	 * Creates a model object for this extension
	 *
	 * @param string $name
	 * @return \PS\Extempore\Modeler\TCAModel
	 */
	public function &model($name) {
		$model = new Modeler\TCAModel($name);
		$this->models[] = $model;
		return $model;
	}

	/**
	 * Finalizes the TCA by sending the modeler ($this)
	 * to the TCA expander.
	 */
	public function done() {
		global $TCA;
		$expander = new Expander\TCA();
		$TCA = $expander->expand($this);
	}
}